<?php 

	header('Content-type: application/json');

	$received = json_decode(file_get_contents('php://input'));

	function connect(){

		$servername = "localhost";
		$username = "mysongs_user";
		$password = "mysongs_rocks";

		try {
	    $conn = new PDO("mysql:host=$servername;dbname=mysongs", $username, $password);
	    // set the PDO error mode to exception
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    // echo "\nConnected successfully\n";
	  } catch(PDOException $e) {
	    echo "\nConnection failed: " . $e->getMessage()."\n";
	  }

	  return $conn;
	}

	function listSongs($conn){
		$queryStr = "SELECT * FROM songs ORDER BY id DESC";

		$list = [];
		foreach ($conn->query($queryStr) as $row) array_push($list, $row);
		
		return $list;	
	}


	/*-------------LOGIC STARTS HERE-------------*/
	$conn = connect();
	if(!$conn) return;

	$list = listSongs($conn);	

	echo json_encode($list);

?>