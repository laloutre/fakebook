<?php 
	header('Content-type: application/json');

	// $received = file_get_contents('php://input');
	$received = json_decode(file_get_contents('php://input'));

	function connect(){

		$servername = "localhost";
		$username = "mysongs_user";
		$password = "mysongs_rocks";

		try {
	    $conn = new PDO("mysql:host=$servername;dbname=mysongs", $username, $password);
	    // set the PDO error mode to exception
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    // echo "\nConnected successfully\n";
	  } catch(PDOException $e) {
	    echo "\nConnection failed: " . $e->getMessage()."\n";
	  }

	  return $conn;
	}

	function getSong($conn,$id){
		$queryStr = "SELECT * FROM songs WHERE id='$id'";
		foreach ($conn->query($queryStr) as $row) $song = $row;

		$parts = [];
		$queryStr = "SELECT * FROM parts WHERE song_id='$id'";
		foreach ($conn->query($queryStr) as $part) array_push($parts, $part);
		
		$song['parts'] = $parts;
		return $song;	
	}


	/*-------------LOGIC STARTS HERE-------------*/
	$conn = connect();
	if(!$conn) return;

	$list = getSong($conn,$received->id);	

	echo json_encode($list);
	
	// echo '{"test":"neeee"}'	
?>