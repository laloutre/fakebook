<?php 
	header('Content-type: application/json');

	// $received = file_get_contents('php://input');
	$received = json_decode(file_get_contents('php://input'));

	function connect(){

		$servername = "localhost";
		$username = "mysongs_user";
		$password = "mysongs_rocks";

		try {
	    $conn = new PDO("mysql:host=$servername;dbname=mysongs", $username, $password);
	    // set the PDO error mode to exception
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    // echo "\nConnected successfully\n";
	  } catch(PDOException $e) {
	    echo "\nConnection failed: " . $e->getMessage()."\n";
	  }

	  return $conn;
	}

	function getSong($conn,$id){
		$queryStr = "SELECT * FROM songs WHERE id='$id'";
		foreach ($conn->query($queryStr) as $row) $song = $row;

		// $out = [];
		// $out = (object) array('infos' => '');
		$out = new stdClass();
		$out->infos = new stdClass();

		$out->infos->id = $song['id'];
		$out->infos->title = $song['title'];
		$out->infos->artist = $song['artist'];
		$out->infos->album = $song['album'];
		$out->infos->transcription = $song['author'];
		$out->infos->style = $song['style'];
		$out->infos->tonality = $song['tonality'];
		$out->infos->signature = $song['signature'];
		$out->infos->owner = $song['owner'];


		// $path = '../shared/scores/public/'.$id.'.json';
		// json_decode( file_get_contents($path, json_encode($score)) );
		// // file_get_contents($path, $score);


		$path = '../shared/scores/public/'.$id.'.json';
    $parts;
		$score = json_decode( file_get_contents($path, $parts) );

		$out->infos->youtube_link = $score->infos->youtube_link;
		$out->infos->youtube_videoId = $score->infos->youtube_videoId;
		$out->infos->youtube_filePath = $score->infos->youtube_filePath;
		$out->infos->clef = $score->infos->clef;
		$out->parts = $score->parts;
		return $out;	
	}

	function getUserFolder($conn,$usr){

		$mail 	= $usr->user_email;
		$nick 	= $usr->userNick;
		$pass = $usr->user_pass;

		$queryStr = "SELECT folder FROM user WHERE email='$mail' AND nick='$nick' AND pass='$pass'";
		echo $queryStr;
		$ids = [];
		foreach ($conn->query($queryStr) as $row) array_push($ids, $row['id']);

		return $ids;

	}	

	/*-------------LOGIC STARTS HERE-------------*/
	$conn = connect();
	if(!$conn) return;

	$list = getSong($conn,$received->id);	

	echo json_encode($list);
	
	// echo '{"test":"neeee"}'	
?>