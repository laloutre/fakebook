<?php 

	header('Content-type: application/json');
	$received = json_decode(file_get_contents('php://input'));



  $infos 		= $received->infos;
  $parts 		= $received->parts;
  $usr 			= $received->usr;
  
  //checks for empty parts.
	if($infos->title == ""){
		echo "Title empty ! abording save"; 
		return; 
	}

	$conn = connect();

	$song_id = insertOrUpdate($conn, $infos, $parts, $usr);

	if($song_id == ''){
		echo 'you cant save that score...';
		exit;
	}
	$score = new stdClass();
	$score->infos = $infos;
	$score->parts = $parts;

	file_put_contents('../shared/scores/public/'.$song_id.'.json', json_encode($score));

	// error_log("LE SONG ID en save est : ".$song_id);
	echo $song_id;
	// $conn->close();
	// echo $received->title;



	//********CONNECTION*****************************
	function connect(){

		$servername = "localhost";
		$username = "mysongs_user";
		$password = "mysongs_rocks";

		try {
	    $conn = new PDO("mysql:host=$servername;dbname=mysongs", $username, $password);
	    // set the PDO error mode to exception
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    // echo "\nConnected successfully\n";
	  } catch(PDOException $e) {
	    echo "\nConnection failed: " . $e->getMessage()."\n";
	  }

	  return $conn;
	}
	//***********************************************



	function insertOrUpdate($conn, $infos, $parts, $usr){

		// if($id == -1) $result = getScoreId($conn, $infos);

		$result = getScoreById($conn, $infos->songId);

		if (count($result) == 0) {
	    // echo "\nNew score : creating\n";
	    $song_id = insertSong($conn, $infos, $usr);
			// $song_id = getScoreId($conn, $infos)[0];
			// insertParts($conn, $song_id, $parts);
		} else {
	    // echo "\nScore already exists : updating\n";
	    $owner = getScoreOwner($conn,$result[0]);
	    $id = getUserId($conn, $usr);
	    if($id == $owner) $song_id = updateSong($conn, $infos, $usr);
			else echo 'you dont own that score !';
		}

		return $song_id;

	}

	function getScoreId($conn, $infos){

		$queryStr = "SELECT id FROM songs WHERE title='$infos->title' AND artist='$infos->artist' AND author='$infos->transcription' AND style='$infos->style'";

		$ids = [];
		foreach ($conn->query($queryStr) as $row)
			array_push($ids, $row['id']); 

		return $ids;
	}

	function getScoreOwner($conn, $id){

		$queryStr = "SELECT owner FROM songs WHERE id='$id'";

		$owner = [];
		foreach ($conn->query($queryStr) as $row)
			array_push($owner, $row['owner']); 

		return $owner[0];
	}

	function getScoreById($conn, $id){

		$queryStr = "SELECT id FROM songs WHERE id='$id'";

		$ids = [];
		foreach ($conn->query($queryStr) as $row)
			array_push($ids, $row['id']); 

		return $ids;
	}

	function getUserId($conn,$usr){

		$mail 	= $usr->user_email;
		$nick 	= $usr->userNick;
		$folder = $usr->user_folder;
		$pass 	= $usr->user_pass;

		$queryStr = "SELECT id FROM user WHERE email='$mail' AND nick='$nick' AND folder='$folder' AND pass='$pass'";
		$ids = [];
		foreach ($conn->query($queryStr) as $row) array_push($ids, $row['id']);

		return $ids[0];

	}	

	function getUserFolder($conn,$usr){

		$mail 	= $usr->user_email;
		$nick 	= $usr->userNick;
		$pass = $usr->user_pass;

		$queryStr = "SELECT folder FROM user WHERE email='$mail' AND nick='$nick' AND pass='$pass'";
		$ids = [];
		foreach ($conn->query($queryStr) as $row) array_push($ids, $row['id']);

		return $ids;

	}	

	function insertSong($conn, $infos, $usr){

		$title = $infos->title;
		$artist = $infos->artist;
		$album = $infos->album;
		$author = $usr->userNick;
		$style = $infos->style;
		$tonality = $infos->tonality;
		$signature = $infos->signature;
		$owner = getUserId($conn,$usr);
		// error_log("THE OWNER IS : ".$owner);

		$infos->transcription = $author;

		// if (count($owner) != 1) {
		// 	// error
		// } 
		$owner = $owner[0];

		$queryStr = "INSERT INTO songs(title,artist,album,author,style,tonality,signature,owner) VALUES('$title', '$artist', '$album' ,'$author', '$style', '$tonality', '$signature','$owner' )";

		$nb = $conn->exec($queryStr);
		if ( $nb ) {
	    // echo "\nNew record created ".$nb." successfully.\n";
			$id = getScoreId($conn, $infos)[0];
			return $id;
			// echo $id;

		} else {
	    return "\nError: " . $queryStr . "\n" . $conn->error;
		}
	}



	function updateSong($conn, $infos,$usr){

		$id = $infos->songId;
		$title = $infos->title;
		$artist = $infos->artist;
		$album = $infos->album;
		$author = $usr->userNick;
		$style = $infos->style;
		$tonality = $infos->tonality;

		$statement = $conn->prepare("UPDATE songs SET title=:title WHERE id=:id");
		if ($statement === false) {
			echo 'erreur statement';
		  return;
		}
		$statement->bindParam('title', $title, PDO::PARAM_STR);	
		$statement->bindParam('id', $id, PDO::PARAM_INT);	
		$statement = $statement->execute();


		//***********************
		$statement = $conn->prepare("UPDATE songs SET artist=:artist WHERE id=:id");
		if ($statement === false) {
			echo 'erreur statement';
		  return;
		}
		$statement->bindParam('artist', $artist, PDO::PARAM_STR);	
		$statement->bindParam('id', $id, PDO::PARAM_INT);	
		$statement = $statement->execute();


		//***********************

		$statement = $conn->prepare("UPDATE songs SET album=:album WHERE id=:id");
		if ($statement === false) {
			echo 'erreur statement';
		  return;
		}
		$statement->bindParam('album', $album, PDO::PARAM_STR);	
		$statement->bindParam('id', $id, PDO::PARAM_INT);	
		$statement = $statement->execute();

		//***********************

		$statement = $conn->prepare("UPDATE songs SET author=:author WHERE id=:id");
		if ($statement === false) {
			echo 'erreur statement';
		  return;
		}
		$statement->bindParam('author', $author, PDO::PARAM_STR);	
		$statement->bindParam('id', $id, PDO::PARAM_INT);	
		$statement = $statement->execute();

		//***********************

		$statement = $conn->prepare("UPDATE songs SET style=:style WHERE id=:id");
		if ($statement === false) {
			echo 'erreur statement';
		  return;
		}
		$statement->bindParam('style', $style, PDO::PARAM_STR);	
		$statement->bindParam('id', $id, PDO::PARAM_INT);	
		$statement = $statement->execute();

		//***********************

		$statement = $conn->prepare("UPDATE songs SET tonality=:tonality WHERE id=:id");
		if ($statement === false) {
			echo 'erreur statement';
		  return;
		}
		$statement->bindParam('tonality', $tonality, PDO::PARAM_STR);	
		$statement->bindParam('id', $id, PDO::PARAM_INT);	
		$statement = $statement->execute();

		return $id;
	}
?>
