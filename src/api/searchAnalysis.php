<?php 
	header('Content-type: application/json');
	$received = json_decode(file_get_contents('php://input'));

	function connect(){

		$servername = "localhost";
		$username = "mysongs_user";
		$password = "mysongs_rocks";

		try {
	    $conn = new PDO("mysql:host=$servername;dbname=mysongs", $username, $password);
	    // set the PDO error mode to exception
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    // echo "\nConnected successfully\n";
	  } catch(PDOException $e) {
	    echo "\nConnection failed: " . $e->getMessage()."\n";
	  }

	  return $conn;
	}

	function searchParts($conn,$pattrn,$type){

		// this pattern for chords =>   
		// ("[^"]*\/{0,1}Bb")\s*(?:"_[VIvi][^|"]*"){0,1}[^|"]*[|]{0,1}[^|"]*("[^"]*\/{0,1}Cm")\s*(?:"_[VIvi][^|"]*"){0,1}[^|"]*[|]{0,1}[^|"]*("[^"]*\/{0,1}B")

		// this pattern is for analysis	
		// ("_I")[^|"]* [|]{0,1} \s* ("[A-G][^|"]*"){0,1} [^|"]* ("_V")

		$chords = explode(" ", $pattrn);
		if(sizeOf($chords) == 0)return [];

		if($type == 'chords'){

			$regPart2 = '\s*("_[VIvi][^|"]*"){0,1}[^|"]*[|]{0,1}[^|"]*';

		}else if($type == 'analysis'){

			$regPart2 = '[^|"]*[|]{0,1}\s*("[ABCDEFG][^"]*"){0,1}[^|]*';
		}

		if($type == 'chords'){

			$regex = '("[^"]*/{0,1}' .$chords[0].'")';

		}else if($type == 'analysis'){

			$regex = '("_'.$chords[0].'")';
		}
		
		for($i = 1; $i < sizeOf($chords); $i++){

			$regex = $regex . $regPart2;

			if($type == 'chords')
				$regex = $regex . '("[^"]*/{0,1}' .$chords[$i]. '[^"]*")';
			else if($type == 'analysis')
				$regex = $regex . '("_'.$chords[$i].'")';
		}

		// echo $regex;

		$queryStr = "SELECT * FROM parts WHERE abc REGEXP '$regex'";
		$matches = [];	

		foreach ($conn->query($queryStr) as $part){

			$sg_id = $part['song_id'];

			$queryStr2 = "SELECT * FROM songs WHERE id='$sg_id'";
			foreach ($conn->query($queryStr2) as $match){
				$match['part_title'] = $part['title'];
				unset($match['nb_parts']);
				array_push($matches, $match);
			}
		}
		
		return $matches;	
	}

	function searchSongs($conn,$pattrn,$table){

		$queryStr = "SELECT * FROM `songs` WHERE `$table` REGEXP '($pattrn)'";
		// echo $queryStr;
		$matches = [];	
		foreach ($conn->query($queryStr) as $song) array_push($matches, $song);

		return $matches;	
	}
	/*-------------LOGIC STARTS HERE-------------*/
	$conn = connect();
	if(!$conn) return;

	$searchType = $received->type;
	$pattrn = $received->pattrn;
	$match = [];

	switch($searchType){

		case('analysis'):
			$match = searchParts($conn,$pattrn,'analysis');	
		break;

		case('chords'):
			$match = searchParts($conn,$pattrn,'chords');	
		break;

		case('title'):
			$match = searchSongs($conn,$pattrn,'title');	
		break;

		case('artist'):
			$match = searchSongs($conn,$pattrn,'artist');	
		break;

		case('author'):
			$match = searchSongs($conn,$pattrn,'author');	
		break;

		case('style'):
			$match = searchSongs($conn,$pattrn,'style');	
		break;

		default:
		break;
	}

	echo json_encode($match);
?>