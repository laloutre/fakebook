<?php
	header('Content-type: application/json');
	$received = json_decode(file_get_contents('php://input'));

	$usermail 		= $received->user_email;
	$password 		= $received->user_pass;


	function connect(){

		$servername = "localhost";
		$username = "mysongs_user";
		$password = "mysongs_rocks";

		try {
	    $conn = new PDO("mysql:host=$servername;dbname=mysongs", $username, $password);
	    // set the PDO error mode to exception
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    // echo "\nConnected successfully\n";
	  } catch(PDOException $e) {
	    echo "\nConnection failed: " . $e->getMessage()."\n";
	  }

	  return $conn;
	}	

	function get_user_by_mail($conn,$usermail){
		$queryStr = "SELECT * FROM user WHERE email='$usermail'";
		foreach ($conn->query($queryStr) as $row) $user = $row;

		return $user;	
	}

	function check_existing_user($conn,$usermail){
		$result = get_user_by_mail($conn, $usermail);

		if (count($result) == 0) 
			return false;
		else
			return true;
	}

	function get_nick($conn,$usermail){
		$queryStr = "SELECT nick FROM user WHERE email='$usermail'";

		$nick = $conn->query($queryStr);
		// echo $queryStr;

		return $nick;
	}

	function get_folder($conn,$usermail){
		$queryStr = "SELECT folder FROM user WHERE email='$usermail'";

		$folder = $conn->exec($queryStr);

		return $folder;

	}

	function log_user($conn,$usermail,$password){

		if(check_existing_user($conn,$usermail) == false)return "error";

		$queryStr = "SELECT * FROM user WHERE email='$usermail' AND pass='$password'";
		$found = [];
		foreach ($conn->query($queryStr) as $row){
		 array_push($found, $row);
		}

		if ( sizeof($found) == 1 ) {

			$nick 	= $found[0]['nick'];
			$folder = $found[0]['folder'];
			$email = $found[0]['email'];

			$object = new stdClass();
			
		  $object->folder = $folder;
		  $object->email = $email;
		  $object->nick = $nick;
			return json_encode($object);

		} else {
	    return "\nError: ";
		}
	
	}

	//-------------LOGIC STARTS HERE 

	$conn = connect();
	if(!$conn) return;

	$return = log_user($conn,$usermail,$password,$nick);	
	echo $return;

	?>