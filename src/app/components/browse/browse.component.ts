import { Component, OnInit } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';

import { UserModel } from '@models/usermodel/usermodel'

import { environment } from '../../../environments/environment'

@Component({
  selector: 'app-browse',
  templateUrl: './browse.component.html',
  styleUrls: ['./browse.component.scss']
})

export class BrowseComponent implements OnInit {

	public list:Array<any> = [];	
	public analysisPatrn:string = '';

	public modalInfos:any={};

	public searchPattrn:any={};

  constructor( public _http:HttpClient, public user:UserModel ) { }

  ngOnInit() {

		this.searchPattrn.title='';
		this.searchPattrn.artist='';
		this.searchPattrn.author='';
		this.searchPattrn.style='';
		this.searchPattrn.chords='';
		this.searchPattrn.analysis='';

  	this.refreshScores();

		this.modalInfos.hide = true;
  }

  refreshScores(){
		const headers = new HttpHeaders({ 'Content-Type': 'application/json'});  
		this._http.get(environment.apiList, {responseType: 'text', headers})
		    .map(res => {
		    	this.list = JSON.parse(res);
			  })
		    .subscribe(
		      data => console.log('data  : '+data),
		      err => console.log(err),
		      () => {console.log('Refresh Complete') }
		    );

  }

  deleteScore(){

  	let id:any = this.modalInfos['id'];
		const headers = new HttpHeaders({ 'Content-Type': 'application/json'});  

		let obj:object = {
			id:id,
			user:this.user.get_user()
		};

		this._http.post(environment.apiDeleteSong, obj, {responseType: 'text', headers})
	    .map(res => {
	    	this.refreshScores();
		  	this.modalInfos.hide = true;	
		  })
	    .subscribe(
	      data => console.log('data  : '+data),
	      err => {
		      console.log(err),
  	    	this.refreshScores();
  		  	this.modalInfos.hide = true;	
	      },
	      () => {console.log('delete Complete') }
	    );
  }

  search(type:string='title'){

  	if(this.searchPattrn.title == '' && this.searchPattrn.artist == '' && this.searchPattrn.author == '' && 
  			this.searchPattrn.style == '' && this.searchPattrn.chords == '' && this.searchPattrn.analysis == '' ){
  		this.refreshScores();
  		return;
  	}
		let obj:object = {type:type,pattrn:this.searchPattrn[type]};
		console.log('obj => ',obj);

		const headers = new HttpHeaders({ 'Content-Type': 'application/json'});  
		this._http.post(environment.apiSearchAnalysis, obj, {responseType: 'text', headers})
			  .map(res => {
			  	try{	
			    	this.list = JSON.parse(res);
			    	console.log("this.list => ",this.list);
			    }catch(e){
				  	console.log('res => ',res);
			    }
			  })
			  .subscribe(
			    data => console.log('data  : '+data),
			    err => console.log(err),
			    () => {console.log('Search Complete') }
			  );
  }

  openDeleteModal(index:number){

  	this.modalInfos.id = this.list[index].id;
  	this.modalInfos.title 	= this.list[index].title;	
  	this.modalInfos.hide 	= false;	
  }

}
