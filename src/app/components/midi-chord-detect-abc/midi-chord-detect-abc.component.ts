import { Component, OnInit, Input, OnDestroy, NgZone } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { ChordDetectService } from "@services/chord-detect/chord-detect.service"

import { Chord } from "@tonaljs/tonal";
import { Note } from "@tonaljs/tonal";
import { AbcNotation } from "@tonaljs/tonal";


import abcjs from 'abcjs';
declare global {
  var abcjs:any;
}

@Component({
  selector: 'app-midi-chord-detect-abc',
  templateUrl: './midi-chord-detect-abc.component.html',
  styleUrls: ['./midi-chord-detect-abc.component.scss']
})
export class MidiChordDetectAbcComponent implements OnInit, OnDestroy{

  @Input() tonality:string="C";

	public chords:Array<string>;
  private midiNotesTab$:Subscription;

  constructor(private zone:NgZone, private chord_detect:ChordDetectService) { 

    this.midiNotesTab$ = this.chord_detect.abc_chords.subscribe( (chords_obj) =>{ //Subscription to chord detection service.
      this.renderStrings(chords_obj.l,chords_obj.r);
    })
  }

  ngOnInit(): void {
  	this.renderStrings('','');
  }

  ngOnDestroy(){
    this.midiNotesTab$.unsubscribe();
  }

  renderStrings(l,r){
    let header = '';
    header += "%%musicspace 50"+"\n";
    header += "%%sysstaffsep 50"+"\n";
    header += "%%staffsep 100"+"\n";
    header += "%%staves {(PianoRightHand) (PianoLeftHand)}"+"\n";
    header += "V:PianoRightHand clef=treble down"+"\n";
    header += "V:PianoLeftHand clef=bass"+"\n";
    header += "K:C"+"\n";
    header += "[V: PianoLeftHand] x"+"\n";
    header += "[V: PianoRightHand] x"+"\n";
    header += "K:"+this.tonality+"\n";

    l = "[V: PianoLeftHand] "+"["+l+"]2 x"+"\n";
    r = "[V: PianoRightHand] "+"["+r+"]2 x"+"\n";

    abcjs.renderAbc('midi-chord-detect-abc', header+l+r, {
      paddingtop:1,
      paddingbottom:1,
      paddingright:0,
      paddingleft:0,
      responsive: "resize",
      staffwidth:100,
      canvas_id: 'abcChord' 
    });
  }

}
