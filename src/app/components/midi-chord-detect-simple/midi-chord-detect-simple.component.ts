import { Component, OnInit, NgZone, Input, Output,EventEmitter } from '@angular/core';

import { Subscription } from 'rxjs/Subscription';

import {MusicUtilsService} from "../../services/music-utils-service/music-utils.service";
import {MidiService } from "../../services/midi/midi.service";

import { ChordDetectService } from "@services/chord-detect/chord-detect.service"

// import * as $ from "jquery"

@Component({
	selector: '[app-midi-chord-detect-simple]',
	templateUrl: './midi-chord-detect-simple.component.html',
	styleUrls: ['./midi-chord-detect-simple.component.scss']
})

export class MidiChordDetectSimpleComponent {

	public chord:any ={};

	public divHidden = false;

	public chord_name:string="";
	public midi_chord:Subscription;

	constructor(private zone:NgZone, private chord_detect:ChordDetectService) { 

		this.midi_chord = this.chord_detect.abc_chords.subscribe( (chords_obj) =>{ //Subscription to chord detection service.
	    this.zone.run(()=>{
	    	this.chord_name = chords_obj.chords[0]; // avant j'utilisais mu.getChord(notes,this.tonality) qui renvoyait un oject plus complet.
	    }); 
	  });
	}

ngOnDestroy(){
	this.midi_chord.unsubscribe();
}	

}
