import { Component, OnInit, NgZone } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';


import { MidiService } from '@services/midi/midi.service';

@Component({
  selector: 'app-midi-selector',
  templateUrl: './midi-selector.component.html',
  styleUrls: ['./midi-selector.component.scss']
})
export class MidiSelectorComponent implements OnInit {

	private plugged_inputs$:Subscription;
  private chosen_input$:Subscription;

  public midi_ports:any=[];
  public selected_midi:string='';

  constructor( private midi: MidiService, private zone:NgZone ) { 

  	this.plugged_inputs$ = this.midi.plugged_inputs$.subscribe((data)=>{
      this.midi_ports = data;
  	});

    this.chosen_input$ = this.midi.chosen_input$.subscribe((data)=>{
      if(!data.length) return;
      this.zone.run(()=>{
        this.selected_midi = data[0].value.name;
        // console.log("this.selected_midi => ",this.selected_midi);
      })

    });

  }

  ngOnInit(): void {}

  change_midi_port(event): void {
    this.midi.bindMidiInput(event.target.value);
  }
}
