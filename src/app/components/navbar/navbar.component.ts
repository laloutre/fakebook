import { Component, OnInit } from '@angular/core';
import { Router  } from '@angular/router';
import { UserModel } from '@models/usermodel/usermodel';
import { environment } from '../../../environments/environment'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit{

  public user_email:string;
  public user_pass:string;
  public passverif:string;
  public user_nick:string;
  public newScoreLink:string;

  constructor(public user:UserModel, public router:Router ) { }

  ngOnInit(){
    this.newScoreLink = environment.newScore;
  }

  login(){
    this.user.set_user_email(this.user_email);
    this.user.login(this.user_pass);
  }

  logout(){
    this.user.logout();
  }

  signUp(){
    if(this.passverif == this.user_pass){ 
      this.user.set_user_email(this.user_email);
      this.user.set_user_nick(this.user_nick);
      
      this.user.sign_up(this.user_pass);
    }else throw "erreur passwords are differents";

  }


}
