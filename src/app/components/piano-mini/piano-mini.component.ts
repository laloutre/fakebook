import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-piano-mini',
  templateUrl: './piano-mini.component.html',
  styleUrls: ['./piano-mini.component.scss']
})
export class PianoMiniComponent implements OnInit {

  @Input() notes: any;
  @ViewChild('minipiano') myId: ElementRef;

  constructor() {}

  ngOnInit(): void {}

  ngAfterViewInit(){
    for(let n of this.notes){
      if( n.indexOf('#') != -1 || n.indexOf('b') != -1 ){
        let nt = n.replace('#','s');
        $(this.myId.nativeElement).find("[id*="+nt+"]").css('fill','green');
      }else{
        $(this.myId.nativeElement).find('#'+n).css('fill','green');
      }
    }

  }

}
