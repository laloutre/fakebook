import { Component, NgZone, OnInit, ElementRef } from '@angular/core';
import { MidiService } from "../../services/midi/midi.service";
import { Subscription } from 'rxjs/Subscription';

import { SoundfontService } from '@services/soundfont/soundfont.service';

@Component({
  selector: 'app-piano',
  templateUrl: './piano.component.html',
  styleUrls: ['./piano.component.scss']
})
export class PianoComponent implements OnInit {

	public display:boolean=false;
	public notes:Array<any>=[];
	public midiNotes$:Subscription;

  constructor( public ms:MidiService, private zone:NgZone, public elRef:ElementRef, public sf:SoundfontService ) { 

	  this.midiNotes$ = this.ms.notesTabSubject.subscribe((notes) => {
      this.zone.run(()=>{ 
	  		this.lightsOff();
	  		this.light(notes);
       });
	  });
  }

  public lightsOff(){
  	this.notes = new Array(108);

  	for(let i = 21; i <= 108; i++) this.notes[i] = 0;

  }

  ngOnDestroy(){
    this.midiNotes$.unsubscribe();
  }

  public light(notes){
  	for(let n of notes) this.notes[n]=1;
  }

  ngOnInit() {
  	this.lightsOff();
  }

  public eventHandlerForChord(e){
  	// console.log("e => ",e);
  }

  /*
  play the note clicked
   */
  public play(e){
    let id = e.target.attributes.id.nodeValue;
    let midiNote = Number(id.replace('note_',''));

    this.sf.playMidiNote(midiNote);

    this.light([midiNote]);
    setTimeout(()=>{
      this.lightsOff();
    },800)
  }
}
