import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';

import { Songmodel } from '@models/songmodel/songmodel';
import { ConfigModel } from '@models/configmodel/configModel';
import { SelectionModel } from '@models/selectionmodel/selectionmodel';
import { DisplayService } from '@services/display/displayService';
import { MinimalRenderService } from '@services/display/minimal-render.service';
import { BindingsService,KEYS } from '@services/bindings/bindings.service';

import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';

import { TransportService } from '@services/transport/transport.service';

import { AudioPlayer } from '@services/audioplayer/audioplayer.service';

import abcjs from 'abcjs';
declare global {
  var abcjs:any;
}


@Component({
  selector: 'app-song',
  templateUrl: './song.component.html',
  styleUrls: ['./song.component.scss']
})
export class SongComponent implements OnInit, OnDestroy,AfterViewInit {

  public sub:Subject<number>;
  private abcString:string;
  private abcString$:Subscription;
  private selectionUpdate$:Subscription;
  public cur_chord:string="C4"
    
  private binding$:any; //Observable ..
  private binding_shift$:any; //Observable ..

  private paper:any; //reference to canvas

  nb_measures_per_line:any=[];
  follow_on:boolean=true;
  private measure:Subject<number>;

  constructor(public route: ActivatedRoute, 
              private sm:Songmodel, 
              public sel:SelectionModel,
              public cm:ConfigModel, 
              public transport:TransportService, 
              public dm:DisplayService, 
              private ap:AudioPlayer, 
              private miniRender:MinimalRenderService, 
              private keys:BindingsService
              ) { }

  ngOnInit() {
    // console.log("SONG COMPONENT : ngOnInit");

    this.measure = new Subject();
    this.measure.next(1);  

    this.abcString$ = this.dm.abcString$.subscribe(abcString=>{
      // console.log('abcString received in song part: '+abcString);
      this.abcString = abcString;
      this.renderAbcWithOptions();
    });

    this.init_bindings();

    this.selectionUpdate$ = this.sel.selected_Update$.subscribe(data=>{

      this.disLight_all();

      if(!data || !data[0])return;
      
      let hash = this.sm.getMeasures_hash();

      // try{

        for(let i=0; i < data.length; i++){
          if(data[i].getType() == 'part')continue;

          // debugger
          // let line = data[i].lig;
          // let pos = data[i].pos;
          let line = hash[data[i].getIdx()].lig;
          let pos = hash[data[i].getIdx()].pos

          this.highlightMeasure(pos,line);
        }
      // }catch(e){
      //   debugger
      // }

    });

    // this.transport.measureChange.subscribe(
    //     data => {
    //       // if(this.follow_on) {
    //       //   let x = this.getMeasureLine(data%this.sm.getMeasures().length);
    //       //   console.log("getMeasureLine => ");
    //       //   console.log(x);
    //       //   this.highlightMeasure(x.measure,x.line); 
    //       // }
    //       // console.log("MEASURE CHANGED !");
    //     },
    //     error => { console.log('Error : ', error); }
    //   );
  }
  public savePrefs():void{
   this.cm.saveInCookie();
  }

  ngOnDestroy() {
    // console.log("SONG COMPONENT : DESTROY");
    this.savePrefs();
    this.abcString$.unsubscribe();
    this.binding$.unsubscribe()
    this.measure.unsubscribe();
    this.selectionUpdate$.unsubscribe();
  }

  ngAfterViewInit(){
    // console.log("SONG COMPONENT INIT ngAfterViewInit");
    this.dm.renderFromModel();
    this.paper = document.querySelector("#abcCanvas");
  }

  /**
   * get the line of the measure m
   * Number of measures per line is given by this.nb_measures_per_line of form [n1,n2,...nx]
   * nx being the number of measures for each line.
   * 
   * @param {[int]} m measure number
   */
  getMeasureLine(m){
    console.log("get measure line m => ",m);
    // console.log("this.nb_measures_per_line => ",this.nb_measures_per_line);

    for(var i = 0; i < this.nb_measures_per_line.length; i++){
      //if this measure number is > number of measures of this line
      if(m >= this.nb_measures_per_line[i]){
        m -= this.nb_measures_per_line[i];
        continue;
      }
      if(m <= this.nb_measures_per_line[i]){
        // if(i==0)m+=1;
       return {measure:m, line:i};
       }
    }
    return {measure:0, line:0};
  }

  measureClicked(abcelem, tuneNumber, classes, analysis, drag, mouseEvent){
    console.log('measureClicked');


    let m = Number( classes.match(/abcjs-m([0-9]+)/)[1] );
    let l = Number( classes.match(/abcjs-l([0-9]+)/)[1] );

    let H = this.sm.getMeasures_hash();
    let M=1;
    for(var h of Object.getOwnPropertyNames(H)){
      if( (H[h].lig == l && H[h].pos == m )|| (H[h].lig == l && H[h].pos == m )){
        M=Number(h);
        break;
      }
    }
    let measure = this.sm.getPart( this.sm.getMeasures_hash()[M].part ).getMeasure( this.sm.getMeasures_hash()[M].meas );
    this.sel.setSelection([ measure ]);
    this.transport.setMeasure(M-1);
    this.transport.setBeat(Number(-1));
  }


  disLight_all(){
    this.paper = document.querySelector("#abcCanvas");

    //Change back red elements to black.
    let elements;
    elements = document.querySelectorAll('.abcjsNoteSelected') as any;

    for(let i of elements){
      i.classList.remove('abcjsNoteSelected');
    }

  }

  highlightMeasure(m,l){
    //color new highlighted elements in red.
    let elements = this.paper.querySelectorAll('.abcjs-chord.abcjs-m'+m+'.abcjs-l'+l+','+'.abcjs-annotation.abcjs-m'+m+'.abcjs-l'+l) as any;

    if(!elements.length){
      console.warn('songComponent: highLightMeasure => Nothing to select : returning.');
      return;
    }

    for(let e of elements){
      e.classList.add('abcjsNoteSelected');
    }


    if(this.cm.isAutoScroll()){
     let elements = this.paper.querySelectorAll( '.abcjs-clef.abcjs-l'+l );
     // abcjs-staff-extra abcjs-clef abcjs-l3 abcjs-m0 abcjs-mm14 abcjs-v0
     if(!elements.length){
       console.warn( "error : song component highlightMeasure() => no elements to scroll to." );
       return;
     }
     this.checkInView(elements,false);
   }

  }

  checkInView(elem,partial)
  {
    // console.log("checkInView");
    var container = $(".song");
    var contHeight = container.height();
    var contTop = container.scrollTop();
    var contBottom = contTop + contHeight ;

    var elemTop = $(elem).offset().top - container.offset().top;
    var elemBottom = elemTop + $(elem).height();
    
    // console.log("elemTop => ",elemTop);
    // console.log("elemBottom => ",elemBottom);

    var isIn = ( elemTop <=screen.height - (screen.height*0.4) && elemTop > 100)

    if(isIn)return; //if into view
    // console.log('ELEMENT NOT IN VIEW : SCROLLING ');

    let behavior = ( this.ap.getState() ) ?'instant':'smooth';
    elem[0].scrollIntoView({
      behavior: 'instant',
      block: 'center'
    });
  }

  onResize(){
   this.renderAbcWithOptions(); 
  }

  renderAbcWithOptions(){

    if( this.cm.minimalRendering ){

      this.miniRender.render('abcCanvas');

      let canvas = document.getElementById('abcCanvas');
      canvas.style.overflow='hidden';
      canvas.style.height='';


    }else{

      // console.log("this.abcString => ",this.abcString);
      setTimeout(()=>{

        let staffwidth = (window.innerWidth>900)?window.innerWidth*0.6: window.innerWidth*0.9;
        
        abcjs.renderAbc('abcCanvas', this.abcString, {
          visualTranspose:this.sm.getTranspose(),
          canvas_id: 'abcCanvas', 
          staffwidth:staffwidth, 
          add_classes:true, 
          clickListener:this.measureClicked.bind(this),
          responsive:true,
          // viewportVertical:true,
          format:{
            partskipfac:1.5,
            partsbox: true,
            partsspace:180,
          }
        });
      },10)    
    }
  }

  private init_bindings():void{

    this.binding$ = this.keys.match( [ KEYS.ESCAPE, KEYS.E ] , []).subscribe(() => {


      let e:any= event;
      //======================================
      //FOR NOW CHECK IF WE ARE ON THE SONG SCREEN
      if(e.path[0].tagName == "INPUT")return;
      let inEditor = $(document.activeElement).parents('.song').length || $(document.activeElement).hasClass('song');
      if(!inEditor) return;  
      e.stopPropagation();
      e.preventDefault();
      //=======================================


      switch(e.keyCode){
        case KEYS.ESCAPE :
          this.cm.setEditor_visible(false);
          this.cm.setMetro_visible(false);
          this.cm.setCircle_visible(false);
          this.cm.setDico_visible(false);
          this.cm.setOptions_visible(false);
        break;

        case KEYS.E : 
          if(e.path[0].tagName == "INPUT")return;
          this.cm.setEditor_visible(true);
          break;
      }
    });
  }

}
