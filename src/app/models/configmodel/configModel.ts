import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie';

@Injectable()
export class ConfigModel {

	private autoScroll:boolean=true;

  private follow:boolean=true;

  public minimalRendering:boolean = false;

  private displayMode:'chords'|'analyse'|'both'='chords';

  public editor_visible:boolean=false;

  public metro_visible:boolean=false;

  public circle_visible:boolean=false;

  public dico_visible:boolean=false;

  public options_visible:boolean=false;

  public chordsBrowse_visible:boolean=false;




  constructor(private cookie:CookieService){


    if( !this.loadFromCookie()  )this.saveInCookie();

  }

public isEditor_visible():boolean{
  return this.editor_visible;
}
public isMetro_visible():boolean{
  return this.metro_visible;
}
public isCircle_visible():boolean{
  return this.circle_visible;
}
public isDico_visible():boolean{
  return this.dico_visible;
}
public isOptions_visible():boolean{
  return this.options_visible;
}
public isChordsBrowse_visible():boolean{
  return this.chordsBrowse_visible;
}


public setEditor_visible(onOff:boolean){
  this.editor_visible = onOff;
}

public setMetro_visible(onOff:boolean){
  this.metro_visible = onOff;
}

public setCircle_visible(onOff:boolean){
  this.circle_visible = onOff;
}

public setDico_visible(onOff:boolean){
  this.dico_visible = onOff;
}

public setOptions_visible(onOff:boolean){
  this.options_visible = onOff;
}

public setChordsBrowse_visible(onOff:boolean){
  this.chordsBrowse_visible = onOff;
}

public toggleChordsBrowse_visible():boolean{
  this.chordsBrowse_visible = !this.chordsBrowse_visible;
  return this.chordsBrowse_visible;
}

public toggleEditor_visible():boolean{
  this.editor_visible = !this.editor_visible;
  return this.editor_visible;
}

public toggleMetro_visible():boolean{
  this.metro_visible = !this.metro_visible;
  return this.metro_visible;
}

public toggleCircle_visible():boolean{
  this.circle_visible = !this.circle_visible;
  return this.circle_visible;
}

public toggleDico_visible():boolean{
  this.dico_visible = !this.dico_visible;
  return this.dico_visible;
}

public toggleOptions_visible():boolean{
  this.options_visible = !this.options_visible;
  return this.options_visible;
}


  public getOptionsAsObject():Object{

    let obj = {};
    obj['autoScroll'] = this.autoScroll;
    obj['follow'] = this.follow;
    obj['minimalRendering'] = this.minimalRendering;
    obj['displayMode'] = this.displayMode;

    return obj;
  }

  public setOptionsFromObject( obj:Object ):void{

    if(obj.hasOwnProperty('autoScroll')) this.setAutoScroll( obj['autoScroll'] );
    if(obj.hasOwnProperty('follow')) this.setFollow( obj['follow'] );
    if(obj.hasOwnProperty('minimalRendering')) this.minimalRendering = obj['minimalRendering'];
    if(obj.hasOwnProperty('displayMode')) this.displayMode = obj['displayMode'];
  }

  public loadFromCookie():boolean{

    if( !this.cookie.getObject('options') ) return false;

    else this.setOptionsFromObject( this.cookie.getObject('options') )

    return true;
  }

  public saveInCookie():void{
    this.cookie.putObject('options',this.getOptionsAsObject());
  }


	public isAutoScroll(){
		return this.autoScroll;
	}
	setAutoScroll(val:boolean){
		this.autoScroll = val;
	}

  public isFollowOn(){
   return this.follow; 
  }

  public setFollow(val:boolean){
    this.follow = val;
  }

  public getDisplayMode():'chords'|'analyse'|'both'{
    return this.displayMode;
  }
  public setDisplayMode(dm:'chords'|'analyse'|'both'):void{
    this.displayMode = dm;
  }

}
