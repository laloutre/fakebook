
import { Beat } from '@models/songmodel/beat';

//used for youtube region
export interface RegionObject {
  start:number;
  end:number;
};

export class Measure {

	private idx:number=-1;
  private type:string='measure';
	private beats:Array<Beat>
	public eol:boolean = false;
	public collapse:boolean = true;

	public notes:string = "";
	public chords:string = "";
	public lyrics:string = "";
	public analysis:string = "";
	public notes_lh:string = "";

	//optionals
	private audioRegion:RegionObject;
	private meter:string;

	constructor(){
		this.beats = [];	
		// this.addBeat(new Beat());
	}

	public addBeat(x:Beat){
		this.beats.push(x);
	}
	public addNewBeat(){
		let b = new Beat();
		this.beats.push(b);
		return b;
	}

	public render(){

		let str = "";

		if(this.meter && !this.eol)str+="\\\nM:"+this.meter+"\n"
		if(this.meter && this.eol)str+="\nM:"+this.meter+"\n"
		if(!this.meter && this.eol)str+="\n";

		for(let i = 0; i < this.beats.length; i++)
			str += this.beats[i].render();

		str += "|";

		// if(this.eol)str+="\n";
		if(str == "||")str = "| x |"

		return str;
	}

	public renderLeftHand(){

		let str = "";

		if(this.meter && !this.eol)str+="\\\nM:"+this.meter+"\n"
		if(this.meter && this.eol)str+="\nM:"+this.meter+"\n"
		if(!this.meter && this.eol)str+="\n";

		for(let i = 0; i < this.beats.length; i++)
			str += this.beats[i].render_lh();

		str += "|";

		// if(this.eol)str+="\n";
		if(str == "||")str = "| x |"

		return str;
	}

	public renderLyrics(){
		let str = "";
		if(this.eol)str+="\n";

		for(let i = 0; i < this.beats.length; i++)
			str += this.beats[i].getLyrics()+" ";

		str += "|";

		return str;
	}


	//--------getters/setters

	public setMeter(x:string){
		this.meter = x;
	}
	public setBeats(x:Array<Beat>){
		this.beats = x;
	}
	public setIdx(x:number){
		this.idx = x;
	}
  public getType(){
    return this.type;
  }
	public setAudioRegion(x:RegionObject){
		this.audioRegion = x;
	}
	public setAudioRegionStart(x:number){
		if(this.hasOwnProperty('audioRegion'))
			this.audioRegion.start = x;
	}
	public setAudioRegionEnd(x:number){
		if(this.hasOwnProperty('audioRegion'))
			this.audioRegion.end = x;
	}
	public removeAudioRegion(){
		delete this.audioRegion;
	}
	public getIdx(){
		return this.idx;
	}

	public getAudioRegion(){
		return this.audioRegion;
	}
	public getAudioDuration(){
		if(!this.audioRegion)return 0;
		return this.audioRegion.end - this.audioRegion.start;
	}

	public getBeats(){
		return this.beats;
	}
	public setEol(x:boolean){
		this.eol = x;
	}
	public getEol(){
		return this.eol;
	}
	public getMeter(){
		return this.meter;
	}

	public setFromLines(){
		let chords = this.chords.split(' ');
		let notes = this.notes.split(' ');
		let analysis = this.analysis.split(' ');
		let lyrics = this.lyrics.split(' ');
		let notes_lh = this.notes_lh.split(' ');

		let cl = chords.length;
		let nl = notes.length;
		let al = analysis.length;
		let ll = lyrics.length;
		let n_ll = notes_lh.length;

		let nb_beats = Math.max(cl,nl,al,ll,n_ll);

		this.setBeats([]);
		for(let b = 0; b < nb_beats; b++ ){
		  let nb = new Beat();

		  if(b<cl)nb.setChord(chords[b]);
		  if(b<nl)nb.setNotes(notes[b]);
		  if(b<al)nb.setAnalysis(analysis[b]);
		  if(b<ll)nb.setLyrics(lyrics[b]);
		  if(b<n_ll)nb.setNotes_lh(notes_lh[b]);

		  this.addBeat(nb);
		}
	}
	
	public getChordsLine(){
		let l='';
		for(let i=0; i<this.beats.length; i++){
					let c = this.beats[i].getChord();
					if(c!=""){
						l+=c;
						l+=" ";
					}
		}
		l=l.slice(0,-1);
		return l;
	}

	public getLyricsLine(){
		let l='';
		for(let i=0; i<this.beats.length; i++){
					let c = this.beats[i].getLyrics();
					if(c!=""){
						l+=c;
						l+=" ";
					}
		}
		l=l.slice(0,-1);
		return l;
	}

	public getAnalysisLine(){
		let l='';
		for(let i=0; i<this.beats.length; i++){
					let c = this.beats[i].getAnalysis();
					if(c!=""){
						l+=c;
						l+=" ";
					}
		}
		l=l.slice(0,-1);
		return l;
	}

  public setAnalysisLine(l:string){
    this.analysis = l;
  }

  public getNotesLine(){
  	let l='';
  	for(let i=0; i<this.beats.length; i++){
  				let c = this.beats[i].getNotes();
  				if(c!=""){
  					l+=c;
  					l+=" ";
  				}
  	}
  	l=l.slice(0,-1);
  	return l;
  }
  public getNotes_lhLine(){
  	let l='';
  	for(let i=0; i<this.beats.length; i++){
  				let c = this.beats[i].getNotes_lh();
  				if(c!=""){
  					l+=c;
  					l+=" ";
  				}
  	}
  	l=l.slice(0,-1);
  	return l;
  }


	public getCollapse(){
		return this.collapse;
	}
	public setCollapse(x:boolean){
		this.collapse = x;
	}


}
