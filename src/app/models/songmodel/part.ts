import { Measure } from '@models/songmodel/measure';

import { Key } from "@tonaljs/tonal";

export class Part {

	private idx:number=-1;
  private type:string='part';

	private title:string;
	private tonality:string;
	private meter:string;

	private measures:Array<Measure>;

	private measures_hash:any;
	private measures_max_lines:number;

	public collapse:true;

	constructor(){
		this.title = 'New part';
		this.tonality='';
		this.meter='4/4';
		this.measures = [];
	}


	//--------getters/setters
	public getIdx(){
		return this.idx;
	}
	public setIdx(x:number){
		this.idx = x;
	}
	
  public getType(){
    return this.type;
  }

	public getTitle(){
		return this.title;
	}
	public setTitle(x:string){
		this.title = x;
	}

	public getMeasures(){
		return this.measures;
	}
	public getMeasure(x:number){
		return this.measures[x];
	}
	public setMeasures(x:Array<Measure>){
		this.measures = x;
	}
	public addMeasure(x:Measure){
		this.measures.push(x);
	}
	public addMeasureAfter(idx:number,x:Measure){
		this.measures.splice(idx+1,0,x);
	}
	public addNewMeasure(){
		let m = new Measure();
		this.measures.push(m);
		return m;
	}
	public deleteMeasure(m){
		this.measures.splice(m,1);
	}
	public deleteMeasureById(id){

		for(let i = 0; i < this.measures.length; i++){
			if(this.measures[i].getIdx() == id){
				this.deleteMeasure(i);	
			}
		}
	}
	public getLastMeasure(){
		return this.measures.slice(-1)[0];	
	}

	public getTonality(){
		return this.tonality;
	}
	public setTonality(x:string){
		this.tonality = x;
	}


	public getMeter(){
		return this.meter;
	}
	public setMeter(x:string){
		this.meter = x;
	}

	public getMeasures_hash(){
		return this.measures_hash;
	}

	public getMeasures_max_lines(){
		return this.measures_max_lines;
	}

	public renderWithLyrics(){

		let str = "";
		let lyc = "";
    let tab = {};
		for(let i = 0; i < this.measures.length; i++){
			str += this.measures[i].render();
			lyc += this.measures[i].renderLyrics();
			tab[this.measures[i].getIdx()] = {idx:i, part:this.idx, meas:i};
		}

		let count = 0;
		// if(this.measures.length)count = this.measures[0].getIdx();
		
    let str_lyc 	= "|:";
		//render and glue score/lyrics together.

    let str_lines = str.split('\n');
    let lyc_lines = lyc.split('\n');
    //FOR each line in part
    for(var line = 0; line < str_lines.length; line++){
    	let score = str_lines[line];
    	let lyrics = lyc_lines[line];

	  	//replace final bar with :|
    	if(line == str_lines.length-1){
	    	score=score.slice(0,-1);
    		score+=":|";	
    	}

    	// str_lyc += score +"\n"+"w:"+lyrics+"\n"; //temporarily remove lyrics.
      str_lyc += score +"\n";


      // for each measure on this line
    	var nb_m_onLine = (str_lines[line]. match(/\|/g) || []).length;
    	for(let j=0; j<nb_m_onLine;j++){
    		var id=(this.measures[count].getIdx());
    		if(line==0)tab[id]['part_first_line'] = true;
    		tab[id]['lig']=line;
    		tab[id]['pos']=j;
        count++;
    	}
    	// count+=nb_m_onLine;
    }
    this.measures_max_lines = line;
    this.measures_hash = tab;
    // debugger



  	//case of final \n remove it
  	if(str_lyc.slice(-1)=='\n'){
			str_lyc = str_lyc.slice(0,-1);
  	}

  	let out = "P:"+this.title+"\n"
  	out+="K:"+this.tonality+"\n";
  	out+="M:"+this.meter+"\n";
  	out+="[V: V1]"+"\n";
  	out+=str_lyc;
  	// console.log("out => ",out);

		return out;
	}
	public renderWithLeftHand(){

		let str = "";
		let lyc = "";
    let tab = {};
		for(let i = 0; i < this.measures.length; i++){
			str += this.measures[i].render();
			lyc += this.measures[i].renderLyrics();
			tab[this.measures[i].getIdx()] = {idx:i, part:this.idx, meas:i};
		}

		let count = 0;
		if(this.measures.length)count = this.measures[0].getIdx();
		
    let str_lyc 	= "|:";
		//render and glue score/lyrics together.
    let str_lines = str.split('\n');
    let lyc_lines = lyc.split('\n');
    for(var i = 0; i < str_lines.length; i++){
    	let score = str_lines[i];
    	let lyrics = lyc_lines[i];

	  	//replace final bar with :|
    	if(i == str_lines.length-1){
	    	score=score.slice(0,-1);
    		score+=":|";	
    	}

    	// str_lyc += score +"\n"+"w:"+lyrics+"\n"; //temporarily remove lyrics.
      str_lyc += score +"\n";


    	let nb_m_onLine = (str_lines[i]. match(/\|/g) || []).length;
    	for(let j=0; j<nb_m_onLine;j++){
    		let id=(count+j);
    		if(i==0)tab[id]['part_first_line'] = true;
    		tab[id]['lig']=i;
    		tab[id]['pos']=j;
    	}
    	count+=nb_m_onLine;
    }
    this.measures_max_lines = i;
    this.measures_hash = tab;



  	//case of final \n remove it
  	if(str_lyc.slice(-1)=='\n'){
			str_lyc = str_lyc.slice(0,-1);
  	}

  	let out = "P:"+this.title+"\n"
  	out+="K:"+this.tonality+"\n";
  	out+="M:"+this.meter+"\n";
  	out+="[V: V1]"+"\n";
  	out+=str_lyc;
  	// console.log("out => ",out);

		return out;
	}

	public renderLeftHand(){

		let str = "";
		let lyc = "";
    let tab = {};
		for(let i = 0; i < this.measures.length; i++){
			str += this.measures[i].renderLeftHand();
			// lyc += this.measures[i].renderLyrics();
			tab[this.measures[i].getIdx()] = {idx:i, part:this.idx, meas:i};
		}

		let count = 0;
		if(this.measures.length)count = this.measures[0].getIdx();
		
    let str_tot 	= "|:";
		//render and glue score/lyrics together.
    let str_lines = str.split('\n');
    for(var i = 0; i < str_lines.length; i++){
    	let score = str_lines[i];

	  	//replace final bar with :|
    	if(i == str_lines.length-1){
	    	score=score.slice(0,-1);
    		score+=":|";	
    	}

      str_tot += score +"\n";


    	let nb_m_onLine = (str_lines[i]. match(/\|/g) || []).length;
    	for(let j=0; j<nb_m_onLine;j++){
    		let id=(count+j);
    		if(i==0)tab[id]['part_first_line'] = true;
    		tab[id]['lig']=i;
    		tab[id]['pos']=j;
    	}
    	count+=nb_m_onLine;
    }
    this.measures_max_lines = i;
    this.measures_hash = tab;



  	//case of final \n remove it
  	if(str_tot.slice(-1)=='\n'){
			str_tot = str_tot.slice(0,-1);
  	}

  	let out = "P:"+"\n"
  	// out+="K:"+this.tonality+"\n";
  	out+="M:"+this.meter+"\n";
  	out+=str_tot;
  	// console.log("out => ",out);

		return out;
	}
  /*
  compute analysis based on chords and tonality.
  Sets analysis for every measure (delete what's already there).
   */
  public compute_analysis(){

    let degrees; 
    // let diatonic = ['A','B','C','D','E','F','G'];
    // let rot = diatonic.indexOf(this.tonality[0]);

    // console.log(Key.majorKey(this.tonality));
    // this.mu.getDiatonicScale(this.tonality[0]);
    //rotates the array diatonic to get the tonality diatonic scale. Ex :in Dmaj => ["D", "E", "F", "G", "A", "B", "C"]
    // let tonality_diatonic_scale = diatonic.slice(rot, diatonic.length).concat(diatonic.slice(0, rot));

    let isMinor = this.tonality.indexOf('m') >= 0;
    let key;
    let tonality_diatonic_scale;
    if(!isMinor){
      let k = Key.majorKey(this.tonality);
      tonality_diatonic_scale = k.scale;
      degrees = k.grades;

    } else{

      let k = Key.minorKey(this.tonality.replace('m',''));
      tonality_diatonic_scale = k.harmonic.scale;
      degrees = k.harmonic.grades;
    }

    let chordLine = '';
    for(let m of this.measures){

      let skipMeasure;
      chordLine = m.getChordsLine();

      // console.log('------------------');
      // console.log('chordLine => ',chordLine);
      // if(chordLine == '')continue; // if chord line is just a space, continue.

      let root_tab = chordLine.match(/([ABCDEFG][b#]*)/gm);

      if(root_tab == null )continue; //if no chords in measure m, continue.

      // console.log('root_tab => ',root_tab);

      let degrees_tab = [];
      for(let root of root_tab){
        let d = degrees[ tonality_diatonic_scale.indexOf(root) ];
        degrees_tab.push(d);
      }
      // console.log('degrees_tab => ',degrees_tab);

      for(let i=0; i < root_tab.length; i++){
        if(degrees_tab[i]== undefined){
          skipMeasure = true;
          console.warn('can\'t find chord with root : ',root_tab[i],' in tonality of ', this.tonality,' : ',tonality_diatonic_scale, ' skipping measure');
          continue;
        }
        chordLine = chordLine.replace(root_tab[i],degrees_tab[i]);
      }
      // console.log('chordLine => ',chordLine);
      // console.log('degrees_tab => ',degrees_string);
      // debugger
      if(skipMeasure)continue;

      m.setAnalysisLine(chordLine);
      m.setFromLines();
    }
  }
}
