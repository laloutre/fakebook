import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment'
import { BehaviorSubject } from 'rxjs';
import { CookieService } from 'ngx-cookie';

@Injectable({
	providedIn: 'root'
})

export class UserModel {

	public user_email:string;
	public user_nick:string;
	public user_folder:string;
	private user_pass:string;
	// private user_is_logged:boolean;
	public user_is_logged = new BehaviorSubject(false); // inital value is "false"

	constructor(private _http: HttpClient, private cookieService: CookieService ){
		let cookie:any;
		cookie = this.cookieService.getObject('user');
		if(cookie != undefined){
			this.user_email = cookie.user_email;
			this.user_nick = cookie.user_nick;
			this.user_folder = cookie.user_folder;
	    this.user_is_logged.next(true);
	    this.user_pass = cookie.user_pass;
		}


		// CLEAR ALL RESULTS in COOKIE =============DEBUG ONLY=============
		// 
  	// this.clearAllResults();
  	// 
  	// ================================================================
  	// console.log(this.getResults())
	}

	/**
	 * create a new user
	 */
	public sign_up(pass){

		let obj :object = {
			user_email:this.user_email,
			user_nick:this.user_nick,
			user_pass:pass
		};

		if(this.user_email == "" || this.user_nick == "" || pass == "")
			throw "userModel : create() => missing information";
			
		const headers = new HttpHeaders({ 'Content-Type': 'application/json'});  

		this._http.post(environment.apiCreateUser, obj,{responseType: 'text', headers})
		    .map(res => {
		    	console.log("Received : \n"+res);
			  })
		    .subscribe(
		      data => console.log('data  : '+data),
		      err => console.log(err),
		      () => {
		      	console.log('Creating user Complete');
		      }
		    );
	}

	public login(pass){
		let obj :object = {
			user_email:this.user_email,
			user_nick:this.user_nick,
			user_pass:pass
		};

		if(this.user_email == "" || pass == "")
			throw "userModel : sign_in() => missing information";
			
		const headers = new HttpHeaders({ 'Content-Type': 'application/json'});  
		
		this._http.post(environment.apiSignIn, obj,{responseType: 'text', headers})
		    .map(res => {
		    	try{
			    	let data = JSON.parse(res);
				    this.user_nick = data.nick;
				    this.user_email = data.email;
				    this.user_folder = data.folder;
				    this.user_pass = pass;
				    this.user_is_logged.next(true);
				    this.cookieService.putObject('user', {user_nick:this.user_nick,user_email:this.user_email,user_folder:this.user_folder,user_pass:this.user_pass});
			    }catch(e){
			    	console.clear();
			    	console.warn(res);
			    }

			  })
		    .subscribe(
		      data => console.log('data  : '+data),
		      err => console.log(err),
		      () => {console.log('Sign In Complete'); }
		    );

	}

	public logout(){
		this.user_email = '';
		this.user_nick = '';
		this.user_pass = '';
		this.user_is_logged.next(false); 
	}

	public is_logged(){
		return this.user_is_logged;
	}

	public getCookie(key: string){
    return this.cookieService.get(key);
  }

  public addResults(level:string, result:any):any{

    let old_results = this.cookieService.getObject('results');

    if(!old_results)old_results = {};

    if( !old_results.hasOwnProperty(level) ) {
      old_results[level] = [];
    }

    let D = new Date();
    let date = (D.getUTCMonth() + 1) +"-"+ D.getUTCDate()+"-"+ D.getUTCFullYear();


    // if already results for this exo : mean it. else create it.
    if( old_results[level].length && old_results[level][old_results[level].length-1].hasOwnProperty(date)){
      console.log("MEAN OF ",old_results[level][old_results[level].length-1][date], "and ", result)
      old_results[level][old_results[level].length-1][date] = ( old_results[level][old_results[level].length-1][date] + result ) / 2;
    }else{
      old_results[level].push( { [date]: result }) ;
    }
    // old_results[level]["3-7-2021"]= Math.round(Math.random()*100);
    // old_results[level]["3-6-2021"]= Math.round(Math.random()*100);
    // old_results[level]["3-5-2021"]= Math.round(Math.random()*100);
    // old_results[level]["3-4-2021"]= Math.round(Math.random()*100);
    // old_results[level]["3-3-2021"]= Math.round(Math.random()*100);
    // old_results[level]["3-2-2021"]= Math.round(Math.random()*100);

    // this.cookieService.putObject("results",{});
    this.cookieService.putObject("results",old_results);

    return old_results;
  }

  public clearAllResults(){
    this.cookieService.putObject("results",{});
  
  }

  public clearResults(level){
    let old_results = this.cookieService.getObject('results');
    old_results[level] = [];
    this.cookieService.putObject("results",old_results);
 
  }

  public getResults(){

    if(!this.cookieService.getObject('results'))
	    this.cookieService.putObject("results",{});

  	return this.cookieService.getObject('results');	
  }

	public get_folder(){
		return this.user_folder;
	}

	public get_nick(){
		return this.user_nick;
	}

	public set_user_nick(nic){
		this.user_nick = nic;
	}

	public set_user_email(email){
		this.user_email = email;
	}

	public get_user(){
		let usr = {
			userNick:this.user_nick,
			user_email:this.user_email,
			user_folder:this.user_folder,
			user_pass:this.user_pass,
			is_logged:this.is_logged().getValue()
		}
		return usr;
	}


}
