import { Injectable, NgZone } from '@angular/core';

import { Subscription } from 'rxjs/Subscription';
import { BehaviorSubject } from 'rxjs';

import { Chord } from "@tonaljs/tonal";
import { Note } from "@tonaljs/tonal";
import { AbcNotation } from "@tonaljs/tonal";

import {MidiService } from "@services/midi/midi.service";

@Injectable({
  providedIn: 'root'
})
export class ChordDetectService {

  public abc_chords = new BehaviorSubject({chords:[""],l:"",r:""}); 
  private midiNotesTab$:Subscription;


  constructor(private midi:MidiService, private zone:NgZone) {
  	this.midiNotesTab$ = this.midi.notesTabSubject.subscribe((data)=>{
  		this.detectChord_as_abc(data); 
  	})
  }

	detectChord_as_abc(data){
		let both = data.map(Note.fromMidi);
		let chords = Chord.detect(both); 

    let l = []; let r = [];
    let s_l:string=""; let s_r:string="";
    // Separates left and right hand over midi note 60
    data.map((val)=>{
      (val>=60)?r.push(val):l.push(val);
    });

    l = l.map(Note.fromMidi);
    r = r.map(Note.fromMidi);

    s_l = l.map(AbcNotation.scientificToAbcNotation).join('');
    s_r = r.map(AbcNotation.scientificToAbcNotation).join('');

		this.abc_chords.next( {chords:chords,l:s_l,r:s_r} );

	}
}
