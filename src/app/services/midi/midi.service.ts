import { Injectable, NgZone, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs';
import { Subject } from 'rxjs';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})

export class MidiService {

	// public notesTab :any;
	public MIDI_AVAILABLE:boolean = false;
	private midi :any;
	private sustain :boolean = false;

	public notesTab :Array<number>;
	public notesTabSubject :BehaviorSubject<number[]>;

	public plugged_inputs_tab : Array<any> = [];
	public plugged_inputs$ : BehaviorSubject<any[]>;

	public chosen_input  : any;
	public chosen_input$ : BehaviorSubject<any[]>;

	constructor(private zone:NgZone) {
		this.notesTab = new Array<number>();
		this.notesTabSubject = new BehaviorSubject<number[]>([]); // inital value is []

		this.plugged_inputs$ = new BehaviorSubject<any[]>([]); // inital value is []

		this.chosen_input$ = new BehaviorSubject<any[]>([]); // inital value is []

		const requestMIDIAccess = navigator['requestMIDIAccess'];
		if (requestMIDIAccess) {
			navigator['requestMIDIAccess']({
				sysex: false
			}).then(this.onMIDISuccess.bind(this), this.onMIDIFailure);
		} else {
			alert("No MIDI support in your browser.");
		}

	}

	onMIDISuccess(midiAccess) {
		// console.log('MIDI Access Object', midiAccess);
		this.midi = midiAccess;

		this.bindMidiInput();
	}

	bindMidiInput(name:string="Nord Stage 3 MIDI 1"){
		// console.log("binding name => ",name);

		var inputs = this.midi.inputs.values();
		this.plugged_inputs_tab = [];

		for (var input = inputs.next(); input && !input.done; input = inputs.next()) 
			input.value.onmidimessage = null;

		inputs = this.midi.inputs.values();

		// looking for name in midi inputs
		for (var input = inputs.next(); input && !input.done; input = inputs.next()) {
			// console.log("input.value.name => ",input.value.name);

			this.plugged_inputs_tab.push(input);

			if(input.value.name == name) {
    		this.MIDI_AVAILABLE = true;
				input.value.onmidimessage = this.onMIDIMessage.bind(this);
				this.chosen_input = input;
				this.chosen_input$.next([this.chosen_input]);
				// console.log("BOUND => ",input.value.name);
			}
		}
		this.plugged_inputs$.next(this.plugged_inputs_tab); 
	
	}


	onMIDIMessage(event) {

		// const status = event.data[0] & 0xf0;
		const status = event.data[0];
		var filtered = [];

		// debugger
		// Uint8Array(3) [144, 53, 40]
		// Uint8Array(3) [176, 64, 127]
		// Uint8Array(3) [176, 64, 0]

			if(status === 144 ){ 				//noteOn
				// console.log('noteOn : ',event.data[1], "time : "+ performance.now());
				filtered = this.notesTab
				filtered.push(event.data[1]);
				filtered = [...new Set(filtered)];
				filtered.sort(function(a, b){return a - b});

				this.notesTab = filtered;
				this.refreshNotes(filtered);



			}else if(status == 128){//noteOff

				if(this.sustain == true){//dont send noteoffs

				filtered = this.notesTab
				filtered = filtered.filter(function(value,i,a){
					return value != event.data[1];
					});
				filtered = [...new Set(filtered)];
				filtered.sort(function(a, b){return a - b});
				this.notesTab = filtered;

				}else{                    //send noteoffs

					filtered = this.notesTab
					filtered = filtered.filter(function(value,i,a){
						return value != event.data[1];
						});
					filtered = [...new Set(filtered)];
					filtered.sort(function(a, b){return a - b});

					this.notesTab = filtered;
					this.refreshNotes(filtered);
				}

			}else if(status == 176){ //sustain

				if(event.data[2] == 0 && this.notesTab.length ==0){

					this.sustain = false;
					this.notesTab = [];
					this.refreshNotes([]);

				}else if(event.data[2] ==127 ){

					this.sustain = true;

				}else if(event.data[2]==0){
					this.sustain = false;
				}
			}
		}

		clear_notesTabArray(){
			this.notesTab = [];
			this.notesTabSubject.next(this.notesTab);
		}

		refreshNotes(notes){
			// this.zone.run(()=>{this.notesTabSubject.next(notes); })
			this.notesTabSubject.next(notes); 
		}

		onMIDIFailure(e) {
			this.MIDI_AVAILABLE = false;
			console.log(e);
			debugger
		}

	}
